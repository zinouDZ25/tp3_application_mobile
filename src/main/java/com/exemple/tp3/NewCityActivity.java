package com.exemple.tp3;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;


public class NewCityActivity extends AppCompatActivity {

    private EditText textName, textCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_city);

        textName = (EditText) findViewById(R.id.editNewName);
        textCountry = (EditText) findViewById(R.id.editNewCountry);

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO
                //DB HELPER
                WeatherDbHelper DBHElper = new WeatherDbHelper(NewCityActivity.this);
                //to write in the database
                SQLiteDatabase db = DBHElper.getWritableDatabase();
                //Create the city

                HttpURLConnection urlConnection = null;
                JSONResponseHandler json;
                JSONObject JO = null;
                BufferedReader bufferedReader = null;

                try {
                    URL url= WebServiceUrl.build(textName.getText().toString(),textCountry.getText().toString());
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("GET");
                    urlConnection.setDoInput(true);
                    urlConnection.setDoOutput(true);
                    urlConnection.connect();
                    bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuffer stringBuffer = new StringBuffer();
                    String line="";


                    while ((line = bufferedReader.readLine()) != null)
                    {
                        stringBuffer.append(line+"\r\n");
                    }

                    urlConnection.disconnect();


                    try{
                        JO = new JSONObject(stringBuffer.toString());
                        JSONObject coordObj = DBHElper.getObject("coord", JO);
                        JSONArray weatherObj = JO.getJSONArray("weather");
                        JSONObject mainObj = DBHElper.getObject("main",JO);
                        JSONObject windObj = DBHElper.getObject("wind",JO);
                        JSONObject cloudsObj= DBHElper.getObject("clouds",JO);
                        JSONObject sysObj = DBHElper.getObject("sys",JO);
                        int x = (int)Double.parseDouble(mainObj.getString("temp"))- 273;
                        String temp = String.valueOf(x)+" C°";
                        //Create a new object City
                        Date currentTime = Calendar.getInstance().getTime();

                        City city = new City(JO.getLong("id"),JO.getString("name"),sysObj.getString("country"),temp,mainObj.getString("humidity"),windObj.getString("speed"),windObj.getString("deg"),cloudsObj.getString("all"),weatherObj.getJSONObject(0).getString("icon"),weatherObj.getJSONObject(0).getString("description"),currentTime.toString());
                        DBHElper.addCity(city);
                        Toast.makeText(NewCityActivity.this,
                                "Added ! ", Toast.LENGTH_LONG).show();
                    }catch (Exception e){
                        Log.e("Application_BLEM", "Problème : ", e);
                    }

                } catch(MalformedURLException e){
                    e.printStackTrace();
                }catch (IOException e){
                    e.printStackTrace();
                }

            }
        });
    }


}
