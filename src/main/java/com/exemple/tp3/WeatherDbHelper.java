package com.exemple.tp3;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class WeatherDbHelper extends SQLiteOpenHelper {

    private static final String TAG = WeatherDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 11;

    public static final String DATABASE_NAME = "weather.db";

    public static final String TABLE_NAME = "weather";

    public static final String _ID = "_id";
    public static final String COLUMN_CITY_NAME = "city";
    public static final String COLUMN_COUNTRY = "country";
    public static final String COLUMN_TEMPERATURE = "temperature";
    public static final String COLUMN_HUMIDITY = "humidity";
    public static final String COLUMN_WIND_SPEED = "windSpeed";
    public static final String COLUMN_WIND_DIRECTION = "windDirection";
    public static final String COLUMN_CLOUDINESS = "cloudiness";
    public static final String COLUMN_ICON = "icon";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_LAST_UPDATE = "lastupdate";

    public WeatherDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        final String SQL_CREATE_BOOK_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY," +

                COLUMN_CITY_NAME + " TEXT NOT NULL, " +
                COLUMN_COUNTRY + " TEXT NOT NULL, " +
                COLUMN_TEMPERATURE + " INTEGER, " +
                COLUMN_HUMIDITY+ " TEXT, " +
                COLUMN_WIND_SPEED+ " TEXT, " +
                COLUMN_WIND_DIRECTION+ " TEXT, " +
                COLUMN_CLOUDINESS+ " TEXT, " +
                COLUMN_DESCRIPTION+ " TEXT, " +
                COLUMN_ICON+ " TEXT, " +
                COLUMN_LAST_UPDATE+ " TEXT, " +

                // To assure the application have just one weather entry per
                // city name and country, it's created a UNIQUE
                " UNIQUE (" + COLUMN_CITY_NAME + ", " +
                COLUMN_COUNTRY + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_BOOK_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(" drop table if exists "+TABLE_NAME);
        onCreate(db);
    }


    /**
     * Adds a new city
     * @return  true if the city was added to the table ; false otherwise (case when the pair (city name, country) is
     * already in the data base
     */
    public boolean addCity(City city) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_CITY_NAME, city.getName());
        values.put(COLUMN_COUNTRY, city.getCountry());
        values.put(COLUMN_TEMPERATURE, city.getTemperature());
        values.put(COLUMN_HUMIDITY, city.getHumidity());
        values.put(COLUMN_WIND_SPEED, city.getWindSpeed());
        values.put(COLUMN_WIND_DIRECTION, city.getWindDirection());
        values.put(COLUMN_CLOUDINESS, city.getCloudiness());
        values.put(COLUMN_ICON, city.getIcon());
        values.put(COLUMN_DESCRIPTION, city.getDescription());
        values.put(COLUMN_LAST_UPDATE, city.getLastUpdate());

        Log.d(TAG, "adding: "+city.getName()+" with id="+city.getId());

        // Inserting Row
        // The unique used for creating table ensures to have only one copy of each pair (city name, country)
        // If rowID = -1, an error occured
        long rowID = db.insertWithOnConflict(TABLE_NAME, null, values, CONFLICT_IGNORE);
        db.close(); // Closing database connection

        return (rowID != -1);
    }

    /**
     * Updates the information of a city inside the data base
     * @return the number of updated rows
     */
    public int updateCity(City city) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_CITY_NAME, city.getName());
        values.put(COLUMN_COUNTRY, city.getCountry());
        values.put(COLUMN_TEMPERATURE, city.getTemperature());
        values.put(COLUMN_HUMIDITY, city.getHumidity());
        values.put(COLUMN_WIND_SPEED, city.getWindSpeed());
        values.put(COLUMN_WIND_DIRECTION, city.getWindDirection());
        values.put(COLUMN_CLOUDINESS, city.getCloudiness());
        values.put(COLUMN_ICON, city.getIcon());
        values.put(COLUMN_DESCRIPTION, city.getDescription());
        values.put(COLUMN_LAST_UPDATE, city.getLastUpdate());

        // updating row
        return db.updateWithOnConflict(TABLE_NAME, values, _ID + " = ?",
                new String[] { String.valueOf(city.getId()) }, CONFLICT_IGNORE);
    }

    /**
     * Returns a cursor on all the cities of the data base
     */
    public Cursor fetchAllCities() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null,
                null, null, null, null, null, null);

        Log.d(TAG, "call fetchAllCities()");
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    /**
     * Returns a list on all the cities of the data base
     */
    public List<City> getAllCities() {
        List<City> res = new ArrayList<>();
	// TODO
        return res;
    }

    public void deleteCity(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, _ID + " = ?",
                new String[]{cursor.getString(cursor.getColumnIndex(_ID))});
        db.close();
    }

    public void deleteCity(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, _ID + " = ?",
                new String[]{String.valueOf(id)});
        db.close();
    }

    public static JSONObject getObject(String tagName, JSONObject jObj) throws JSONException, JSONException {
        JSONObject subObj = jObj.getJSONObject(tagName);
        return subObj;
    }
    public void populate() {
        Log.d(TAG, "call populate()");
        HttpURLConnection urlConnection = null;
        JSONResponseHandler json;
        JSONObject JO = null;
        BufferedReader bufferedReader = null;
        Date currentTime = Calendar.getInstance().getTime();

        try {
             URL url= WebServiceUrl.build("Avignon","France");
             urlConnection = (HttpURLConnection) url.openConnection();
             urlConnection.setRequestMethod("GET");
             urlConnection.setDoInput(true);
             urlConnection.setDoOutput(true);
             urlConnection.connect();
             bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
             StringBuffer stringBuffer = new StringBuffer();
             String line="";


            while ((line = bufferedReader.readLine()) != null)
            {
                stringBuffer.append(line+"\r\n");
            }

            urlConnection.disconnect();


            try{
                JO = new JSONObject(stringBuffer.toString());
                JSONObject coordObj = getObject("coord", JO);
                JSONArray weatherObj = JO.getJSONArray("weather");

                Log.d(TAG, "call JSONARRAY ICON"+ weatherObj.getJSONObject(0).getString("icon"));



                //JSONObject baseObj = getObject("base",JO);
                JSONObject mainObj = getObject("main",JO);
                //JSONObject visibilityObj = getObject("visibility",JO);
                JSONObject windObj = getObject("wind",JO);
                JSONObject cloudsObj= getObject("clouds",JO);
                //JSONObject dtObj = getObject("dt",JO);
                JSONObject sysObj = getObject("sys",JO);

                //iterator for the keys of the sysobject
                Iterator iterate = sysObj.keys();
                while(iterate.hasNext()){
                    String key = iterate.next().toString();
                    Log.v("KEYS","while keys "+key);
                }
                int x = (int)Double.parseDouble(mainObj.getString("temp"))- 273;


                String temp = String.valueOf(x)+" C°";
                //Create a new object City
City city = new City(JO.getLong("id"),JO.getString("name"),sysObj.getString("country"),temp,mainObj.getString("humidity"),windObj.getString("speed"),windObj.getString("deg"),cloudsObj.getString("all"),weatherObj.getJSONObject(0).getString("icon"),weatherObj.getJSONObject(0).getString("description"),currentTime.toString());
                addCity(city);
            }catch (Exception e){
                Log.e("Application_BLEM", "Problème : ", e);
            }

        } catch(MalformedURLException e){
                    e.printStackTrace();
                    }catch (IOException e){
            e.printStackTrace();
        }

//***************************


        try {
            URL url= WebServiceUrl.build("Paris","France");
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            StringBuffer stringBuffer = new StringBuffer();
            String line="";


            while ((line = bufferedReader.readLine()) != null)
            {
                stringBuffer.append(line+"\r\n");
            }

            urlConnection.disconnect();


            try{
                JO = new JSONObject(stringBuffer.toString());
                JSONObject coordObj = getObject("coord", JO);
                JSONArray weatherObj = JO.getJSONArray("weather");

                Log.d(TAG, "call JSONARRAY ICON"+ weatherObj.getJSONObject(0).getString("icon"));



                //JSONObject baseObj = getObject("base",JO);
                JSONObject mainObj = getObject("main",JO);
                //JSONObject visibilityObj = getObject("visibility",JO);
                JSONObject windObj = getObject("wind",JO);
                JSONObject cloudsObj= getObject("clouds",JO);
                //JSONObject dtObj = getObject("dt",JO);
                JSONObject sysObj = getObject("sys",JO);

                //iterator for the keys of the sysobject
                Iterator iterate = sysObj.keys();
                while(iterate.hasNext()){
                    String key = iterate.next().toString();
                    Log.v("KEYS","while keys "+key);
                }
                int x = (int)Double.parseDouble(mainObj.getString("temp"))- 273;


                String temp = String.valueOf(x)+" C°";
                //Create a new object City
                City city = new City(JO.getLong("id"),JO.getString("name"),sysObj.getString("country"),temp,mainObj.getString("humidity"),windObj.getString("speed"),windObj.getString("deg"),cloudsObj.getString("all"),weatherObj.getJSONObject(0).getString("icon"),weatherObj.getJSONObject(0).getString("description"),currentTime.toString());
                addCity(city);
            }catch (Exception e){
                Log.e("Application_BLEM", "Problème : ", e);
            }

        } catch(MalformedURLException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        //*********************************
        try {
            URL url= WebServiceUrl.build("Rennes","France");
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            StringBuffer stringBuffer = new StringBuffer();
            String line="";


            while ((line = bufferedReader.readLine()) != null)
            {
                stringBuffer.append(line+"\r\n");
            }

            urlConnection.disconnect();


            try{
                JO = new JSONObject(stringBuffer.toString());
                JSONObject coordObj = getObject("coord", JO);
                JSONArray weatherObj = JO.getJSONArray("weather");

                Log.d(TAG, "call JSONARRAY ICON"+ weatherObj.getJSONObject(0).getString("icon"));



                //JSONObject baseObj = getObject("base",JO);
                JSONObject mainObj = getObject("main",JO);
                //JSONObject visibilityObj = getObject("visibility",JO);
                JSONObject windObj = getObject("wind",JO);
                JSONObject cloudsObj= getObject("clouds",JO);
                //JSONObject dtObj = getObject("dt",JO);
                JSONObject sysObj = getObject("sys",JO);

                //iterator for the keys of the sysobject
                Iterator iterate = sysObj.keys();
                while(iterate.hasNext()){
                    String key = iterate.next().toString();
                    Log.v("KEYS","while keys "+key);
                }
                int x = (int)Double.parseDouble(mainObj.getString("temp"))- 273;


                String temp = String.valueOf(x)+" C°";
                //Create a new object City
                City city = new City(JO.getLong("id"),JO.getString("name"),sysObj.getString("country"),temp,mainObj.getString("humidity"),windObj.getString("speed"),windObj.getString("deg"),cloudsObj.getString("all"),weatherObj.getJSONObject(0).getString("icon"),weatherObj.getJSONObject(0).getString("description"),currentTime.toString());
                addCity(city);
            }catch (Exception e){
                Log.e("Application_BLEM", "Problème : ", e);
            }

        } catch(MalformedURLException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        //******************************************
        try {
            URL url= WebServiceUrl.build("Montreal","Canada");
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            StringBuffer stringBuffer = new StringBuffer();
            String line="";


            while ((line = bufferedReader.readLine()) != null)
            {
                stringBuffer.append(line+"\r\n");
            }

            urlConnection.disconnect();


            try{
                JO = new JSONObject(stringBuffer.toString());
                JSONObject coordObj = getObject("coord", JO);
                JSONArray weatherObj = JO.getJSONArray("weather");

                Log.d(TAG, "call JSONARRAY ICON"+ weatherObj.getJSONObject(0).getString("icon"));



                //JSONObject baseObj = getObject("base",JO);
                JSONObject mainObj = getObject("main",JO);
                //JSONObject visibilityObj = getObject("visibility",JO);
                JSONObject windObj = getObject("wind",JO);
                JSONObject cloudsObj= getObject("clouds",JO);
                //JSONObject dtObj = getObject("dt",JO);
                JSONObject sysObj = getObject("sys",JO);

                //iterator for the keys of the sysobject
                Iterator iterate = sysObj.keys();
                while(iterate.hasNext()){
                    String key = iterate.next().toString();
                    Log.v("KEYS","while keys "+key);
                }
                int x = (int)Double.parseDouble(mainObj.getString("temp"))- 273;


                String temp = String.valueOf(x)+" C°";
                //Create a new object City
                City city = new City(JO.getLong("id"),JO.getString("name"),sysObj.getString("country"),temp,mainObj.getString("humidity"),windObj.getString("speed"),windObj.getString("deg"),cloudsObj.getString("all"),weatherObj.getJSONObject(0).getString("icon"),weatherObj.getJSONObject(0).getString("description"),currentTime.toString());
                addCity(city);
            }catch (Exception e){
                Log.e("Application_BLEM", "Problème : ", e);
            }

        } catch(MalformedURLException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        //******************************************
        try {
            URL url= WebServiceUrl.build("Fortaleza","Brazil");
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            StringBuffer stringBuffer = new StringBuffer();
            String line="";


            while ((line = bufferedReader.readLine()) != null)
            {
                stringBuffer.append(line+"\r\n");
            }

            urlConnection.disconnect();


            try{
                JO = new JSONObject(stringBuffer.toString());
                JSONObject coordObj = getObject("coord", JO);
                JSONArray weatherObj = JO.getJSONArray("weather");

                Log.d(TAG, "call JSONARRAY ICON"+ weatherObj.getJSONObject(0).getString("icon"));



                //JSONObject baseObj = getObject("base",JO);
                JSONObject mainObj = getObject("main",JO);
                //JSONObject visibilityObj = getObject("visibility",JO);
                JSONObject windObj = getObject("wind",JO);
                JSONObject cloudsObj= getObject("clouds",JO);
                //JSONObject dtObj = getObject("dt",JO);
                JSONObject sysObj = getObject("sys",JO);

                //iterator for the keys of the sysobject
                Iterator iterate = sysObj.keys();
                while(iterate.hasNext()){
                    String key = iterate.next().toString();
                    Log.v("KEYS","while keys "+key);
                }
                int x = (int)Double.parseDouble(mainObj.getString("temp"))- 273;


                String temp = String.valueOf(x)+" C°";
                //Create a new object City
                City city = new City(JO.getLong("id"),JO.getString("name"),sysObj.getString("country"),temp,mainObj.getString("humidity"),windObj.getString("speed"),windObj.getString("deg"),cloudsObj.getString("all"),weatherObj.getJSONObject(0).getString("icon"),weatherObj.getJSONObject(0).getString("description"),currentTime.toString());
                addCity(city);
            }catch (Exception e){
                Log.e("Application_BLEM", "Problème : ", e);
            }

        } catch(MalformedURLException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        //******************************************
        try {
            URL url= WebServiceUrl.build("Papeete","French Polynesia");
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            StringBuffer stringBuffer = new StringBuffer();
            String line="";


            while ((line = bufferedReader.readLine()) != null)
            {
                stringBuffer.append(line+"\r\n");
            }

            urlConnection.disconnect();


            try{
                JO = new JSONObject(stringBuffer.toString());
                JSONObject coordObj = getObject("coord", JO);
                JSONArray weatherObj = JO.getJSONArray("weather");

                Log.d(TAG, "call JSONARRAY ICON"+ weatherObj.getJSONObject(0).getString("icon"));



                //JSONObject baseObj = getObject("base",JO);
                JSONObject mainObj = getObject("main",JO);
                //JSONObject visibilityObj = getObject("visibility",JO);
                JSONObject windObj = getObject("wind",JO);
                JSONObject cloudsObj= getObject("clouds",JO);
                //JSONObject dtObj = getObject("dt",JO);
                JSONObject sysObj = getObject("sys",JO);

                //iterator for the keys of the sysobject
                Iterator iterate = sysObj.keys();
                while(iterate.hasNext()){
                    String key = iterate.next().toString();
                    Log.v("KEYS","while keys "+key);
                }
                int x = (int)Double.parseDouble(mainObj.getString("temp"))- 273;


                String temp = String.valueOf(x)+" C°";
                //Create a new object City
                City city = new City(JO.getLong("id"),JO.getString("name"),sysObj.getString("country"),temp,mainObj.getString("humidity"),windObj.getString("speed"),windObj.getString("deg"),cloudsObj.getString("all"),weatherObj.getJSONObject(0).getString("icon"),weatherObj.getJSONObject(0).getString("description"),currentTime.toString());
                addCity(city);
            }catch (Exception e){
                Log.e("Application_BLEM", "Problème : ", e);
            }

        } catch(MalformedURLException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        //******************************************
        try {
            URL url= WebServiceUrl.build("Sydney","Australia");
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            StringBuffer stringBuffer = new StringBuffer();
            String line="";


            while ((line = bufferedReader.readLine()) != null)
            {
                stringBuffer.append(line+"\r\n");
            }

            urlConnection.disconnect();


            try{
                JO = new JSONObject(stringBuffer.toString());
                JSONObject coordObj = getObject("coord", JO);
                JSONArray weatherObj = JO.getJSONArray("weather");

                Log.d(TAG, "call JSONARRAY ICON"+ weatherObj.getJSONObject(0).getString("icon"));



                //JSONObject baseObj = getObject("base",JO);
                JSONObject mainObj = getObject("main",JO);
                //JSONObject visibilityObj = getObject("visibility",JO);
                JSONObject windObj = getObject("wind",JO);
                JSONObject cloudsObj= getObject("clouds",JO);
                //JSONObject dtObj = getObject("dt",JO);
                JSONObject sysObj = getObject("sys",JO);

                //iterator for the keys of the sysobject
                Iterator iterate = sysObj.keys();
                while(iterate.hasNext()){
                    String key = iterate.next().toString();
                    Log.v("KEYS","while keys "+key);
                }
                int x = (int)Double.parseDouble(mainObj.getString("temp"))- 273;


                String temp = String.valueOf(x)+" C°";
                //Create a new object City
                City city = new City(JO.getLong("id"),JO.getString("name"),sysObj.getString("country"),temp,mainObj.getString("humidity"),windObj.getString("speed"),windObj.getString("deg"),cloudsObj.getString("all"),weatherObj.getJSONObject(0).getString("icon"),weatherObj.getJSONObject(0).getString("description"),currentTime.toString());
                addCity(city);
            }catch (Exception e){
                Log.e("Application_BLEM", "Problème : ", e);
            }

        } catch(MalformedURLException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        //******************************************
        try {
            URL url= WebServiceUrl.build("Seoul","South Korea");
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            StringBuffer stringBuffer = new StringBuffer();
            String line="";


            while ((line = bufferedReader.readLine()) != null)
            {
                stringBuffer.append(line+"\r\n");
            }

            urlConnection.disconnect();


            try{
                JO = new JSONObject(stringBuffer.toString());
                JSONObject coordObj = getObject("coord", JO);
                JSONArray weatherObj = JO.getJSONArray("weather");

                Log.d(TAG, "call JSONARRAY ICON"+ weatherObj.getJSONObject(0).getString("icon"));



                //JSONObject baseObj = getObject("base",JO);
                JSONObject mainObj = getObject("main",JO);
                //JSONObject visibilityObj = getObject("visibility",JO);
                JSONObject windObj = getObject("wind",JO);
                JSONObject cloudsObj= getObject("clouds",JO);
                //JSONObject dtObj = getObject("dt",JO);
                JSONObject sysObj = getObject("sys",JO);

                //iterator for the keys of the sysobject
                Iterator iterate = sysObj.keys();
                while(iterate.hasNext()){
                    String key = iterate.next().toString();
                    Log.v("KEYS","while keys "+key);
                }
                int x = (int)Double.parseDouble(mainObj.getString("temp"))- 273;


                String temp = String.valueOf(x)+" C°";
                //Create a new object City
                City city = new City(JO.getLong("id"),JO.getString("name"),sysObj.getString("country"),temp,mainObj.getString("humidity"),windObj.getString("speed"),windObj.getString("deg"),cloudsObj.getString("all"),weatherObj.getJSONObject(0).getString("icon"),weatherObj.getJSONObject(0).getString("description"),currentTime.toString());
                addCity(city);
            }catch (Exception e){
                Log.e("Application_BLEM", "Problème : ", e);
            }

        } catch(MalformedURLException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        //******************************************
        try {
            URL url= WebServiceUrl.build("Bamako","Mali");
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            StringBuffer stringBuffer = new StringBuffer();
            String line="";


            while ((line = bufferedReader.readLine()) != null)
            {
                stringBuffer.append(line+"\r\n");
            }

            urlConnection.disconnect();


            try{
                JO = new JSONObject(stringBuffer.toString());
                JSONObject coordObj = getObject("coord", JO);
                JSONArray weatherObj = JO.getJSONArray("weather");

                Log.d(TAG, "call JSONARRAY ICON"+ weatherObj.getJSONObject(0).getString("icon"));



                //JSONObject baseObj = getObject("base",JO);
                JSONObject mainObj = getObject("main",JO);
                //JSONObject visibilityObj = getObject("visibility",JO);
                JSONObject windObj = getObject("wind",JO);
                JSONObject cloudsObj= getObject("clouds",JO);
                //JSONObject dtObj = getObject("dt",JO);
                JSONObject sysObj = getObject("sys",JO);

                //iterator for the keys of the sysobject
                Iterator iterate = sysObj.keys();
                while(iterate.hasNext()){
                    String key = iterate.next().toString();
                    Log.v("KEYS","while keys "+key);
                }
                int x = (int)Double.parseDouble(mainObj.getString("temp"))- 273;


                String temp = String.valueOf(x)+" C°";
                //Create a new object City
                City city = new City(JO.getLong("id"),JO.getString("name"),sysObj.getString("country"),temp,mainObj.getString("humidity"),windObj.getString("speed"),windObj.getString("deg"),cloudsObj.getString("all"),weatherObj.getJSONObject(0).getString("icon"),weatherObj.getJSONObject(0).getString("description"),currentTime.toString());
                addCity(city);
            }catch (Exception e){
                Log.e("Application_BLEM", "Problème : ", e);
            }

        } catch(MalformedURLException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }

    public City cursorToCity(Cursor cursor) {
        City city = new City(cursor.getLong(cursor.getColumnIndex(_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_CITY_NAME)),
                cursor.getString(cursor.getColumnIndex(COLUMN_COUNTRY)),
                cursor.getString(cursor.getColumnIndex(COLUMN_TEMPERATURE)),
                cursor.getString(cursor.getColumnIndex(COLUMN_HUMIDITY)),
                cursor.getString(cursor.getColumnIndex(COLUMN_WIND_SPEED)),
                cursor.getString(cursor.getColumnIndex(COLUMN_WIND_DIRECTION)),
                cursor.getString(cursor.getColumnIndex(COLUMN_CLOUDINESS)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ICON)),
                cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)),
                cursor.getString(cursor.getColumnIndex(COLUMN_LAST_UPDATE))
        );
        return city;
    }

    public City getCity(int id) {
        City city=null;
	// TODO
	return city;
    }
    /**
     * fetch a book from the ID
     * @param id
     * @return Cursor
     */
    public Cursor fetchCity(String id){
        SQLiteDatabase db = this.getReadableDatabase();

        String [] cols= new String[] {_ID,COLUMN_CITY_NAME,COLUMN_COUNTRY,COLUMN_TEMPERATURE,COLUMN_HUMIDITY,COLUMN_WIND_SPEED,COLUMN_WIND_DIRECTION,COLUMN_CLOUDINESS,COLUMN_ICON,COLUMN_DESCRIPTION,COLUMN_LAST_UPDATE};
        Cursor mCursor =db.query(TABLE_NAME,cols,"_id=?",new String[] {id},null,null,null,null);
        if(mCursor!=null){
            mCursor.moveToFirst();
        }
        return mCursor;
    }
}
