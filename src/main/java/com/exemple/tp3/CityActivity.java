package com.exemple.tp3;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;


public class CityActivity extends AppCompatActivity {

    private static final String TAG = CityActivity.class.getSimpleName();
    private TextView textCityName, textCountry, textTemperature, textHumdity, textWind, textCloudiness, textLastUpdate;
    private ImageView imageWeatherCondition;
    private City cityP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        cityP = getIntent().getExtras().getParcelable("myCustomerObj");

        textCityName = (TextView) findViewById(R.id.nameCity);
        textCountry = (TextView) findViewById(R.id.country);
        textTemperature = (TextView) findViewById(R.id.editTemperature);
        textHumdity = (TextView) findViewById(R.id.editHumidity);
        textWind = (TextView) findViewById(R.id.editWind);
        textCloudiness = (TextView) findViewById(R.id.editCloudiness);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageWeatherCondition = (ImageView) findViewById(R.id.imageView);

        updateView();

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
            // TODO
                //DB HELPER
                WeatherDbHelper DBHElper = new WeatherDbHelper(CityActivity.this);
                //to write in the database
                SQLiteDatabase db = DBHElper.getWritableDatabase();
                Date currentTime = Calendar.getInstance().getTime();
                HttpURLConnection urlConnection = null;
                JSONResponseHandler json;
                JSONObject JO = null;
                BufferedReader bufferedReader = null;

                try {
                    URL url = WebServiceUrl.build(textCityName.getText().toString(), textCountry.getText().toString());
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("GET");
                    urlConnection.setDoInput(true);
                    urlConnection.setDoOutput(true);
                    urlConnection.connect();
                    bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuffer stringBuffer = new StringBuffer();
                    String line = "";


                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuffer.append(line + "\r\n");
                    }

                    urlConnection.disconnect();


                    try {
                        JO = new JSONObject(stringBuffer.toString());
                        JSONObject coordObj = DBHElper.getObject("coord", JO);
                        JSONArray weatherObj = JO.getJSONArray("weather");
                        JSONObject mainObj = DBHElper.getObject("main", JO);
                        JSONObject windObj = DBHElper.getObject("wind", JO);
                        JSONObject cloudsObj = DBHElper.getObject("clouds", JO);
                        JSONObject sysObj = DBHElper.getObject("sys", JO);
                        int x = (int) Double.parseDouble(mainObj.getString("temp")) - 273;
                        String temp = String.valueOf(x) + " C°";
                        //Create a new object City
                        City city = new City(cityP.getId(), JO.getString("name"), sysObj.getString("country"), temp, mainObj.getString("humidity"), windObj.getString("speed"), windObj.getString("deg"), cloudsObj.getString("all"), weatherObj.getJSONObject(0).getString("icon"), weatherObj.getJSONObject(0).getString("description"), currentTime.toString());
                        DBHElper.updateCity(city);
                        cityP=city;
                        Toast.makeText(CityActivity.this,
                                "UPDATED ! ", Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Log.e("Application_BLEM", "Problème : ", e);
                    }

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }


                updateView();

            }
        });
     }



    private void updateView() {

        textCityName.setText(cityP.getName());
        textCountry.setText(cityP.getCountry());
        textTemperature.setText(cityP.getTemperature());
        textHumdity.setText(cityP.getHumidity()+" %");
        textWind.setText(cityP.getFullWind());
        textCloudiness.setText(cityP.getHumidity()+" %");
        textLastUpdate.setText(cityP.getLastUpdate());

        if (cityP.getIcon()!=null && !cityP.getIcon().isEmpty()) {
            Log.d(TAG,"icon="+"icon_" + cityP.getIcon());
            imageWeatherCondition.setImageDrawable(getResources().getDrawable(getResources()
                    .getIdentifier("@drawable/"+"icon_" + cityP.getIcon(), null, getPackageName())));
            imageWeatherCondition.setContentDescription(cityP.getDescription());
        }

    }



}
