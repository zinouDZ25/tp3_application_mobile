package com.exemple.tp3;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.os.StrictMode;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        final WeatherDbHelper dbHelper = new WeatherDbHelper(this);
        final SQLiteDatabase db = dbHelper.getReadableDatabase();
       // dbHelper.onUpgrade(db, 9, 9);
        dbHelper.populate();
        final SimpleCursorAdapter dataAdapter = new SimpleCursorAdapter(this,
                R.layout.row,
                dbHelper.fetchAllCities(),
                new String[]{WeatherDbHelper.COLUMN_ICON, WeatherDbHelper.COLUMN_CITY_NAME, WeatherDbHelper.COLUMN_COUNTRY, WeatherDbHelper.COLUMN_TEMPERATURE},
                new int[]{R.id.imageViewRow, R.id.cName, R.id.cCountry, R.id.temperature});
        dataAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {
            @Override
            public boolean setViewValue(View view, Cursor cursor,
                                        int columnIndex) {
                if (view.getId() == R.id.imageViewRow) {

                    String im = cursor.getString(columnIndex);
                    Log.v("*************", "******** " + im);

                    String url = "icon_" + im;
                    int imageToBeShown = getResources().getIdentifier(url,
                            "drawable", getPackageName());


                    ((ImageView) view).setImageResource(imageToBeShown);
                    return true;
                }
                return false;
            }

        });
        ListView listView = (ListView) findViewById(R.id.listCity);
        //set the data in the list View
        listView.setAdapter(dataAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Position +1 because the first position in the database started from 1
                int positionD = position + 1;


                //fetch for a city from the position Item selected
                Cursor cursor = dbHelper.fetchCity(String.valueOf(positionD));
                //Create the city from the Cursor
                City city = dbHelper.cursorToCity(cursor);
                // show toast the id of this city
                Toast.makeText(MainActivity.this,
                        "" + city.getId(), Toast.LENGTH_LONG).show();
                //Create Intent to go to the second ativity cityActivity and send the Object city => myCustomerObj
                Intent intent = new Intent(MainActivity.this, CityActivity.class).putExtra("myCustomerObj", city);
                intent.putExtra("AddCity", false);
                //Start the Second Activity
                startActivity(intent);
            }
        });
        //Add the new City
        FloatingActionButton ajouterC = (FloatingActionButton)findViewById(R.id.addCityFloat);

        ajouterC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Start the BookActivity to Add the City
                Intent intent = new Intent(MainActivity.this,NewCityActivity.class).putExtra("AddCity",true);
                startActivity(intent);
            }
        });

    }



    @Override
    public void onRestart() {
        super.onRestart();
        //When BACK BUTTON is pressed, the activity on the stack is restarted
        //Do what you want on the refresh procedure here
        WeatherDbHelper  dbHelper=new WeatherDbHelper(this);

        //The simpleCursorAdapter with the title of the book and the authors .
        final SimpleCursorAdapter dataAdapter = new SimpleCursorAdapter(this,
                R.layout.row,
                dbHelper.fetchAllCities(),
                new String[] {WeatherDbHelper.COLUMN_ICON,WeatherDbHelper.COLUMN_CITY_NAME,WeatherDbHelper.COLUMN_COUNTRY,WeatherDbHelper.COLUMN_TEMPERATURE},
                new int[] {R.id.imageViewRow,R.id.cName,R.id.cCountry,R.id.temperature });
        dataAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {
            @Override
            public boolean setViewValue(View view, Cursor cursor,
                                        int columnIndex) {
                if(view.getId() == R.id.imageViewRow){

                    String im=cursor.getString(columnIndex);

                    String url ="icon_"+im;
                    int imageToBeShown = getResources().getIdentifier(url,
                            "drawable", getPackageName());



                    ((ImageView)view).setImageResource(imageToBeShown);
                    return true;
                }
                return false;
            }

        });
        ListView listView = (ListView) findViewById(R.id.listCity);

        //set the data in the list View
        listView.setAdapter(dataAdapter);
    }




}
